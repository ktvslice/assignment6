import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application
value ='Welcome!'
search = []
stats = []


def get_db_creds():
    db = os.environ.get("RDS_DB_NAME", None)
    username = os.environ.get("RDS_USERNAME", None)
    password = os.environ.get("RDS_PASSWORD", None)
    hostname = os.environ.get("RDS_HOSTNAME", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE films(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year TEXT, title TEXT, director TEXT, actor TEXT, release_date TEXT, rating FLOAT, title_lower TEXT, actor_lower TEXT, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()
    cur.execute("INSERT INTO films (year, title, director, actor, release_date, rating, title_lower, actor_lower) values ('2018', 'Default Movie', 'A director', 'An Actor', 'April 21, 2018', 5.5, 'default movie', 'an actor')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT * FROM films")
    entries = []
    for row in cur.fetchall():
        movie = {'id':row[0], 'year':row[1], 'title':row[2], 'director':row[3], 'actor':row[4], 'release_date':row[5], 'rating':row[6]}
        entries.append(movie)
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None

def refresh():
    global search
    global value
    global stats
    search = []
    value = 'Welcome!'
    stats = []



# @app.route('/add_to_db', methods=['POST'])
# def add_to_db():
#     print("Received request.")
#     print(request.form['message'])
#     msg = request.form['message']

#     db, username, password, hostname = get_db_creds()

#     cnx = ''
#     try:
#         cnx = mysql.connector.connect(user=username, password=password,
#                                       host=hostname,
#                                       database=db)
#     except Exception as exp:
#         print(exp)
#         import MySQLdb
#         cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

#     cur = cnx.cursor()
#     cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
#     cnx.commit()
#     return hello()

@app.route('/add_movie', methods=['POST'])
def add_movie():
    refresh()
    global value 
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    title_lower = title.lower()
    director = request.form['director']
    actor = request.form['actor']
    actor_lower = actor.lower()
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("INSERT INTO films (year, title, director, actor, release_date, rating, title_lower, actor_lower) values ('" + year + "', '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', '" + rating + "', '" + title_lower + "', '" + actor_lower + "')")
        cnx.commit()
        value = "Movie " + title + " successfully inserted."
    except Exception as exp:
        value = "Movie " + title + " could not be inserted - " + str(exp)
    return hello()


@app.route('/update_movie', methods=['POST'])
def update_movie():
    refresh()
    global value
    print("Received request.")
    year = request.form['year']
    title = request.form['title']
    title_lower = title.lower()
    director = request.form['director']
    actor = request.form['actor']
    actor_lower = actor.lower()
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("UPDATE films SET year = '" + year + "', title = '" + title + "', director = '" + director + "', actor = '" + actor + "', release_date = '" + release_date + "', rating = '" + rating + "', title_lower = '" + title_lower + "', actor_lower = '" + actor_lower + "' WHERE title_lower = '" + title_lower + "' AND year = '" + year + "'")
        cnx.commit()
        value = "Movie " + title + " successfully updated."
    except Exception as exp:
        value = "Movie " + title + " could not be updated - " + str(exp)
    return hello()


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    refresh()
    global value
    print("Received request.")
    title = request.form['delete_title']
    title_lower = title.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    # Check if movie with given title exists
    cur = cnx.cursor()
    cur.execute("SELECT title FROM films WHERE title_lower='" + title_lower + "'")
    result = cur.fetchall()
    value = result
    if result == []:
        value = "Movie with " + title + " does not exist."
    else:
        try:
            cur = cnx.cursor()
            cur.execute("DELETE FROM films WHERE title_lower='" + title_lower + "'")
            cnx.commit()
            value = "Movie " + title + " successfully deleted."
        except Exception as exp:
            value = "Movie " + title + " could not be deleted - " + str(exp)
    
    return hello()


@app.route('/search_movie', methods=['GET'])
def search_movie():
    refresh()
    global value
    global search
    print("Received request.")
    actor = request.args['search_actor']
    actor_lower = actor.lower()

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor FROM films WHERE actor_lower='" + actor_lower + "'")
    result = cur.fetchall()

    if result == []:
        value = "No movies found for actor " + actor + "."
    else:
        for row in result:
            movie = {'year':row[1], 'title':row[0], 'actor':row[2]}
            search.append(movie)
    
    return hello()


@app.route('/highest_rating', methods=['GET'])
def highest():
    refresh()
    global stats
    global value
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM films ORDER BY rating DESC")
    result = cur.fetchall()

    if result == []:
        value = "No movies found."
    else:
        stats.append({'title':result[0][0], 'year':result[0][1], 'actor':result[0][2], 'director':result[0][3], 'rating':result[0][4]})
        index = 1
        while index < len(result):
            if result[index][4] == result[0][4]:
                stats.append({'title':result[index][0], 'year':result[index][1], 'actor':result[index][2], 'director':result[index][3], 'rating':result[index][4]})
                index += 1
            else:
                break

    return hello()


@app.route('/lowest_rating', methods=['GET'])
def lowest():
    refresh()
    global stats
    global value
    print("Received request.")
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM films ORDER BY rating")
    result = cur.fetchall()

    if result == []:
        value = "No movies found."
    else:
        stats.append({'title':result[0][0], 'year':result[0][1], 'actor':result[0][2], 'director':result[0][3], 'rating':result[0][4]})
        index = 1
        while index < len(result):
            if result[index][4] == result[0][4]:
                stats.append({'title':result[index][0], 'year':result[index][1], 'actor':result[index][2], 'director':result[index][3], 'rating':result[index][4]})
                index += 1
            else:
                break

    return hello()


@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries, message=value, search=search, stats=stats)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
